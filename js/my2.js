// Оптимизация не сравнивать с уже отсортированными элементами(- indexOut)
// Параметр для обратной сортировки(reverse)
// Использование деструктуризации,
// что бы не создавать переменную для запоминания временного значения

let array = [10, 5, 7, 80, -50, 25, 100, 78];

function bubbleSort(arr, reverse = false) {
	let { length } = arr;

	for (let indexOut = 0; indexOut <= length - 1; indexOut++) {
		for (
			let indexInner = 0;
			indexInner <= length - indexOut - 1;
			indexInner++
		) {
			let current = indexInner;
			let next = indexInner + 1;

			if (reverse ? arr[current] < arr[next] : arr[current] > arr[next]) {
				[arr[current], arr[next]] = [arr[next], arr[current]];
			}
		}
	}
	return arr;
}

console.log(bubbleSort(array));
