// Поиск энного наибольшего элемента в массиве

function bubbleSortBigAfterBig(arr, position) {
	const { length } = arr;

	for (let i = 0; i < position; i++) {
		for (let j = 0; j < length - 1 - i; j++) {
			if (arr[j] > arr[j + 1]) {
				let temp = arr[j];
				arr[j] = arr[j + 1];
				arr[j + 1] = temp;
			}
		}
	}

	return arr.at(-position);
}

let array = [10000, 10, 100, 1, 5, 1000, 50];
let result = bubbleSortBigAfterBig(array, 2);

console.log(result);
