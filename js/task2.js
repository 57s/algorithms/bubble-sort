// Сортировка строк по их длине

function bubbleSortByStringLength(arr, isReverse = false) {
	const { length } = arr;

	for (let i = 0; i < length; i++) {
		for (let j = 0; j < length - 1 - i; j++) {
			const condition = isReverse
				? arr[j].length < arr[j + 1].length
				: arr[j].length > arr[j + 1].length;

			if (condition) {
				let temp = arr[j];
				arr[j] = arr[j + 1];
				arr[j + 1] = temp;
			}
		}
	}
	return arr;
}

let array = ['123', '1', '12', '0123456789', '12345', '1234'];
let result = bubbleSortByStringLength(array, true);

console.log(result);
