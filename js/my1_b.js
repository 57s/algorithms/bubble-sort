// Сначала находит минимальные элементы

let array = [10, 9, 8, 7, 6, 5, 4, 3, 2, 1];

function bubbleSort(arr) {
	for (let i = 0; i <= arr.length - 1; i++) {
		for (let j = i + 1; j <= arr.length - 1; j++) {
			if (arr[i] > arr[j]) {
				let temp = arr[j];
				arr[j] = arr[i];
				arr[i] = temp;
			}
		}
	}
	return arr;
}

console.log(bubbleSort(array));
