// Сортировка массива объектов по свойству
function bubbleSortByKey(arr, key) {
	const { length } = arr;

	for (let i = 0; i < length; i++) {
		for (let j = 0; j < length - 1 - i; j++) {
			if (arr[j][key] > arr[j + 1][key]) {
				let temp = arr[j];
				arr[j] = arr[j + 1];
				arr[j + 1] = temp;
			}
		}
	}
	return arr;
}

let array = [
	{
		age: 18,
		salary: 30_000,
	},
	{
		age: 20,
		salary: 20_000,
	},
	{
		age: 50,
		salary: 300_000,
	},
	{
		age: 16,
		salary: 15_000,
	},
	{
		age: 25,
		salary: 100_000,
	},
];
let result = bubbleSortByKey(array, 'salary');

console.log(result);
