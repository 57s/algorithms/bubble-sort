function bubbleSort(arr) {
	const { length } = arr;

	for (let i = 0; i < length; i++) {
		for (let j = 0; j < length - 1 - i; j++) {
			if (arr[j] > arr[j + 1]) {
				let temp = arr[j];
				arr[j] = arr[j + 1];
				arr[j + 1] = temp;
			}
		}
	}
	return arr;
}

let array = [10, 9, 8, 7, 6, 5, 4, 3, 2, 1];
let result = bubbleSort(array);

console.log(...result);
