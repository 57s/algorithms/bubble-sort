// Сначала находит максимальные элементы

let array = [10, 9, 8, 7, 6, 5, 4, 3, 2, 1];

function bubbleSort(arr) {
	for (let i = 0; i <= arr.length - 1; i++) {
		for (let j = 0; j <= arr.length - 1; j++) {
			if (arr[j] > arr[j + 1]) {
				let temp = arr[j + 1];
				arr[j + 1] = arr[j];
				arr[j] = temp;
			}
		}
	}
	return arr;
}

console.log(bubbleSort(array));
