// Callback для универсальной  сортировки

let array = Array(100)
	.fill(null)
	.map(() => Math.floor(Math.random() * 1000) - 1);

let arrayObj = [
	{ name: 'Вася', age: 60, money: 1_000_000 },
	{ name: 'Петя', age: 70, money: 1_000 },
	{ name: 'Катя', age: 18, money: 50_000 },
	{ name: 'Саша', age: 50, money: 40_000 },
	{ name: 'Юля', age: 25, money: 100_000 },
];

function bubbleSort(arr, callback) {
	let result = [...arr];
	let { length } = result;

	for (let indexOut = 0; indexOut < length; indexOut++) {
		for (let indexInner = 0; indexInner < length - indexOut - 1; indexInner++) {
			let current = indexInner;
			let next = indexInner + 1;

			if (callback(result[current], result[next])) {
				[result[current], result[next]] = [result[next], result[current]];
			}
		}
	}
	return result;
}

let result1 = bubbleSort(array, (a, b) => a < b);
let result2 = bubbleSort(array, (a, b) => a > b);
let result3 = bubbleSort(arrayObj, (a, b) => a.age > b.age);
let result4 = bubbleSort(arrayObj, (a, b) => a.name > b.name);
let result5 = bubbleSort(arrayObj, (a, b) => a.money > b.money);

console.log(...result2);
